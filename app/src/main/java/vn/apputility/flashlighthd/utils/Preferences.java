package vn.apputility.flashlighthd.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public class Preferences {
	private final String		PACKAGE					= "com.appboost.money:";
	private final String		NAME					= PACKAGE + "share_preference";
	private final String		ACCESSKEY				= PACKAGE + "accessKey";
	private final String		SECRETKEY				= PACKAGE + "secretKey";
	private final String		USER					= PACKAGE + "user";
	private final String		USER_ID					= PACKAGE + "user_id";
	private final String		NUMBERLOTTERY					= PACKAGE + "numberlottery";

	public SharedPreferences	sharedPreferences		= null;
	public Editor				editor					= null;

	public Preferences(Context context) {
		sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		editor = sharedPreferences.edit();
	}

	public String getAccessKey() {
		return sharedPreferences.getString(ACCESSKEY, null);
	}

	public void setAccessKey(String value) {
		editor.putString(ACCESSKEY, value);
		editor.commit();
	}

	public String getSecretKey() {
		return sharedPreferences.getString(SECRETKEY, null);
	}

	public void setSecretKey(String value) {
		editor.putString(SECRETKEY, value);
		editor.commit();
	}

	public String getUser() {
		return sharedPreferences.getString(USER, null);
	}

	public void setUser(String value) {
		editor.putString(USER, value);
		editor.commit();
	}

	public int getNumberLottery() {
		return sharedPreferences.getInt(NUMBERLOTTERY, 0);
	}

	public void setNumberLottery(int value) {
		editor.putInt(NUMBERLOTTERY, value);
		editor.commit();
	}
}
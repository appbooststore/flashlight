package vn.apputility.flashlighthd.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Parcelable;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import vn.apputility.flashlighthd.R;
import vn.apputility.flashlighthd.model.BaseData;

public class Utils {
    public static void animLayout(Context context, final LinearLayout layoutGone, final LinearLayout layoutVisi) {
        Animation animGone = AnimationUtils.loadAnimation(context, R.anim.anim_gone);
        final Animation animVisi = AnimationUtils.loadAnimation(context, R.anim.anim_visiblity);
        animGone.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                layoutGone.setVisibility(View.GONE);
                layoutVisi.startAnimation(animVisi);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        animVisi.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                layoutVisi.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        layoutGone.startAnimation(animGone);
        layoutGone.setVisibility(View.GONE);

    }

    public static void batteryLevel(Context context, final TextView txt_percent, final TextView txt_tem) {
        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                context.unregisterReceiver(this);
                int rawlevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                float temp = ((float) intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0)) / 10;

                txt_percent.setText(level + "%");
                txt_tem.setText(context.getString(R.string.txt_tem, temp));
            }
        };
        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(batteryLevelReceiver, batteryLevelFilter);
    }

    @SuppressLint("SimpleDateFormat")
    public static String getCurrentTimeStamp(Calendar c) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");// dd/MM/yyyy
        String strDate = sdfDate.format(c.getTime());
        return strDate;

    }

    @SuppressLint("UseValueOf")
    public static Class<?> getFieldGenericType(Field field) {
        if (ParameterizedType.class.isAssignableFrom(field.getGenericType().getClass())) {
            ParameterizedType genericType = (ParameterizedType) field.getGenericType();
            return ((Class<?>) (genericType.getActualTypeArguments()[0]));
        }
        return new Boolean(false).getClass();
    }

    @SuppressWarnings("unchecked")
    public static Object getObjectInJSON(Class<? extends BaseData> dtoClass, JSONObject json_data) {
        Object instance = new Object();
        try {
            instance = dtoClass.newInstance();
            Field[] fis = dtoClass.getFields();
            if (fis != null && fis.length > 0) {
                for (Field fi : fis) {
                    try {
                        if (json_data.has(fi.getName().trim())) {
                            Class<?> fType = fi.getType();
                            if (fType == String.class) {
                                fi.set(instance, json_data.getString(fi.getName().trim()));
                            } else if (fType == int.class) {
                                fi.set(instance, json_data.getInt(fi.getName().trim()));
                            } else if (Collection.class.isAssignableFrom(fType)) {
                                Class<?> fieldGenericType = getFieldGenericType(fi);
                                // System.out.println("field " +
                                // fieldGenericType);
                                if (fieldGenericType.getSuperclass().isAssignableFrom(dtoClass)) {
                                    if (json_data.has(fi.getName())) {
                                        try {
                                            JSONArray jsonArray = json_data.getJSONArray(fi
                                                    .getName());
                                            ArrayList<BaseData> array = new ArrayList<BaseData>();
                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                JSONObject i_json = jsonArray.getJSONObject(i);
                                                Object ob = Utils
                                                        .getObjectInJSON(
                                                                (Class<? extends BaseData>) fieldGenericType,
                                                                i_json);
                                                array.add((BaseData) ob);
                                            }
                                            fi.set(instance, array);
                                        } catch (Exception e) {
                                            writeLog(e);
                                            JSONObject result = json_data.getJSONObject(fi
                                                    .getName());
                                            ArrayList<BaseData> array = new ArrayList<BaseData>();
                                            if (result != null) {
                                                Object ob = Utils
                                                        .getObjectInJSON(
                                                                (Class<? extends BaseData>) fieldGenericType,
                                                                result);
                                                array.add((BaseData) ob);
                                                fi.set(instance, array);
                                            }
                                        }
                                    }
                                }
                            } else {

                                JSONObject result = json_data.getJSONObject(fi.getName());
                                if (result != null) {
                                    Object ob = Utils.getObjectInJSON(
                                            (Class<? extends BaseData>) fType, result);
                                    fi.set(instance, ob);
                                }
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            Utils.writeLog(e);
        }
        return instance;
    }

    public static boolean isEmpty(final String string) {
        try {
            return ((string == null) || "".equals(string) || "null".equals(string));
        } catch (final Exception e) {
            return true;
        }
    }

    public static void share(Context context, String string) {
        try {
            String shareLink = "https://play.google.com/store/apps/details?id="
                    + context.getPackageName();
            Intent sentIntent = new Intent(Intent.ACTION_SEND);
            sentIntent.putExtra(Intent.EXTRA_TEXT, string);
            sentIntent.setType("text/plain");

            final List<Intent> targetedShareIntents = new ArrayList<Intent>();

            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> resInfo = pm.queryIntentActivities(sentIntent, 0);

            for (final ResolveInfo resolveInfo : resInfo) {
                final String packageName = resolveInfo.activityInfo.packageName;
                if (packageName.contains("facebook")
                        || (packageName.contains("twitter") || packageName.contains("mail")
                        || packageName.contains("gm") || packageName.contains("sms"))) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName(packageName,
                            resolveInfo.activityInfo.name));
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, string + "\n" + shareLink);
                    intent.putExtra(Intent.EXTRA_STREAM, context.getString(R.string.app_name));
                    intent.setType("text/plain");
                    intent.setPackage(packageName);
                    targetedShareIntents.add(intent);
                }
            }

            Intent openInChooser = Intent.createChooser(targetedShareIntents.remove(0), "Share");
            openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    targetedShareIntents.toArray(new Parcelable[]{}));
            context.startActivity(openInChooser);
        } catch (Exception e) {
            Utils.writeLog(e);
        }

    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        final DisplayMetrics metrics = new DisplayMetrics();
        final Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        display.getMetrics(metrics);
        return metrics;
    }

    public static int getScreenWidth(Context context) {
        return getDisplayMetrics(context).widthPixels;
    }

    public static int getScreenHeight(Context context) {
        return getDisplayMetrics(context).heightPixels;
    }

    public static boolean isNetworkAvailable(Context ctx) {
        int networkStatePermission = ctx
                .checkCallingOrSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE);

        if (networkStatePermission == PackageManager.PERMISSION_GRANTED) {

            ConnectivityManager mConnectivity = (ConnectivityManager) ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo info = mConnectivity.getActiveNetworkInfo();
            if (info == null) {
                return false;
            }
            int netType = info.getType();
            if ((netType == ConnectivityManager.TYPE_WIFI)
                    || (netType == ConnectivityManager.TYPE_MOBILE)) {
                return info.isConnected();
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static String decrypt(String paramString1, String paramString2) throws Exception {
        return new String(decrypt(paramString1.getBytes("UTF-8"),
                Base64.decode(paramString2.getBytes(), 2)));
    }

    private static byte[] decrypt(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
            throws Exception {
        Cipher localCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        localCipher.init(2, new SecretKeySpec(paramArrayOfByte1, "AES"), new IvParameterSpec(
                new byte[16]));
        return localCipher.doFinal(paramArrayOfByte2);
    }

    public static void startOurPlayStore(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("market://search?q=pub: WingApps Studio")));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("https://play.google.com/store/search?q=pub: ")));
        }
    }

    public static void startPlayStore(Context context, String packName) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="
                    + packName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id=" + packName)));
        }
    }

    public static int[] calculateDuration(long duration) {
        try {
            int[] result = new int[2];
            int minutes = (int) (duration / (60));
            duration = duration - minutes * 60;

            int seconds = (int) (duration);

            result[0] = minutes;
            result[1] = seconds;

            return result;
        } catch (Exception e) {
            Utils.writeLog(e);
            return null;
        }

    }

    public static boolean hasHoneyComb() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
    }

    public static final String TAG = "UiUtil";

    public static void setCustomFont(View textViewOrButton, Context ctx, AttributeSet attrs,
                                     int[] attributeSet, int fontId) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, attributeSet);
        String customFont = a.getString(fontId);
        setCustomFont(textViewOrButton, ctx, customFont);
        a.recycle();
    }

    private static boolean setCustomFont(View textViewOrButton, Context ctx, String asset) {
        if (TextUtils.isEmpty(asset))
            return false;
        Typeface tf = null;
        try {
            tf = getFont(ctx, asset);
            if (textViewOrButton instanceof TextView) {
                ((TextView) textViewOrButton).setTypeface(tf);
            } else {
                ((Button) textViewOrButton).setTypeface(tf);
            }
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    private static final Hashtable<String, SoftReference<Typeface>> fontCache = new Hashtable<String, SoftReference<Typeface>>();

    public static Typeface getFont(Context c, String name) {
        synchronized (fontCache) {
            if (fontCache.get(name) != null) {
                SoftReference<Typeface> ref = fontCache.get(name);
                if (ref.get() != null) {
                    return ref.get();
                }
            }

            Typeface typeface = Typeface.createFromAsset(c.getAssets(), "fonts/" + name);
            fontCache.put(name, new SoftReference<Typeface>(typeface));

            return typeface;
        }
    }

    public static String getAndroidId(Context context) {
        String data = "";
        try {
            data = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
        } catch (Exception e) {
            Utils.writeLog(e);
        }
        return data;
    }

    public static void writeLog(Exception e) {
        if (e.getLocalizedMessage() != null)
            Logger.i("myGold ", e.getLocalizedMessage());
        else {
            Logger.i("myGold", e.toString());
        }
    }

    public static boolean isEmail(String s) {
        String pattern = "[a-zA-Z][\\w.]+(@)[\\w]+((.)[\\w]+)+";
        try {
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    public static boolean isTime(int hour1, int minute1, int hour2, int minute2) {
        int result1 = hour1 * 60 + minute1;
        int result2 = hour2 * 60 + minute2;
        // System.out.println("re1,2 " + result1 + "   " + result2);
        if (result1 == 0 && result2 == 0) {
            return true;
        }
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int minute = c.get(Calendar.MINUTE);
        int result = hour * 60 + minute;
        // System.out.println("re " + result);
        if (result >= result1 && result <= result2) {
            return true;
        }
        return false;
    }

    public static boolean isRunningEmulator() {
        boolean isRunning = Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown") || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion");
        if (isRunning) {
            return true;
        }
        isRunning = Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic");
        if (isRunning) {
            return true;
        }
        isRunning = "google_sdk".equals(Build.PRODUCT) || "sdk".equals(Build.PRODUCT)
                || "sdk_x86".equals(Build.PRODUCT) || "vbox86p".equals(Build.PRODUCT);
        String kernelVersion = System.getProperty("os.version");
        if (kernelVersion != null && kernelVersion.contains("x86"))
            isRunning = true;
        return isRunning;
    }

	/*public static String calculateDuration(Context context, long currentDate, long endDate) {
        try {
			String time = "";
			long duration = (endDate - currentDate) / 1000;

			int days = (int) (duration / (60 * 60 * 24));
			duration = duration - days * 60 * 60 * 24;

			int hours = (int) (duration / (60 * 60));
			duration = duration - hours * 60 * 60;

			int minutes = (int) (duration / (60));
			duration = duration - minutes * 60;

			int seconds = (int) (duration);

			if (days > 0) {
				time = String.format(context.getString(R.string.item_time_day), days);
			} else if (hours > 0) {
				time = String.format(context.getString(R.string.item_time_hous), hours);
			} else if (minutes > 0) {
				time = String.format(context.getString(R.string.item_time_minute), minutes);
			} else if (seconds > 0) {
				time = String.format(context.getString(R.string.item_time_secorn), seconds);
			} else {
				time = String.format(context.getString(R.string.item_time_secorn), 0);
			}

			return time;
		} catch (Exception e) {
			writeLog(e);
		}
		return null;
	}*/
/*
    public static void showInfoDialog(Context context, String content) {
		final CustomDialog dialog = new CustomDialog(context, R.layout.custom_dialog_error_connect);
		dialog.setContentDialog(content, false);
		dialog.setCancelable(true);
		dialog.setLeftButton(context.getString(R.string.btn_right), new View.OnClickListener() {

			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}*/

    public static int getVersionCode(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            return pInfo.versionCode;
        } catch (NameNotFoundException e) {
            writeLog(e);
        }
        return -1;
    }

    public static String convertLongToDate(Long mLong) {
        Date date = new Date(mLong);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateText = df2.format(date);
        return dateText;
    }

}

package vn.apputility.flashlighthd.utils;

import android.content.Context;
import android.os.Handler;


import vn.apputility.flashlighthd.model.BaseData;
import vn.apputility.flashlighthd.net.ApiRequest;
import vn.apputility.flashlighthd.net.ApiResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutorService;

import vn.apputility.flashlighthd.model.UpdateDTO;

public class ServerRunable implements Runnable {
    private final String API = "http://api.appsbeez.com/apps/update";
    private ApiAction action = ApiAction.NONE;
    private Context context;
    private ExecutorService executor;
    private CompleteListener listener;

    private Preferences preferences;
    private ApiRequest apiRequest;
    private Handler handler;
    private String mode;


    public interface CompleteListener {
        public void onComplete(Object values);
    }

    public void setListener(CompleteListener listener) {
        this.listener = listener;
    }

    public ServerRunable(Context context, ExecutorService executor) {
        this.context = context;
        this.executor = executor;
        this.preferences = new Preferences(context);
        this.handler = new Handler();
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void getRequet(ApiAction action) {
        this.action = action;
        switch (action) {
            case MODE:
                apiRequest = new ApiRequest(API, null);
                apiRequest.add("mode", "test-force");
                break;
            case NONE:
                apiRequest = new ApiRequest(API, null);
                break;
            default:
                break;
        }
    }

    @Override
    public void run() {
        // System.out.println("action " + action);
        switch (action) {
            case MODE:
                resultHandle(UpdateDTO.class, ApiMethod.GET);
                break;
            case NONE:
                resultHandle(UpdateDTO.class, ApiMethod.GET);
                break;
            default:
                break;

        }
        if (executor != null) {
            executor.shutdown();
        }

    }

    private void resultHandle(Class<? extends BaseData> dtoClass, ApiMethod method) {
        ApiResponse response = null;
        if (method == ApiMethod.POST) {
            response = apiRequest.post();
        } else {
            response = apiRequest.get();
        }
        String init = response.getText();
        int statusCode = response.getStatusCode();
        Object instance = new Object();
        if (statusCode == 200) {
            try {
                Logger.d("init", init);
                instance = Utils.getObjectInJSON(dtoClass, new JSONObject(init));
                handlerPost(instance);
            } catch (JSONException e) {
                handlerPost(null);
            }
        } else {
            handlerPost(null);
        }
    }

    public void handlerPost(final Object object) {
        handler.post(new Runnable() {

            @Override
            public void run() {
                listener.onComplete(object);
            }
        });
    }
}

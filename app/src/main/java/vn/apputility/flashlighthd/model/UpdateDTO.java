package vn.apputility.flashlighthd.model;

/**
 * Created by Phan on 3/4/2016.
 */
public class UpdateDTO extends  BaseData{
    public String update;
    public String force;
    public String link;
    public String version;
}

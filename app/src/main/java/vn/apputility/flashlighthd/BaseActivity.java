package vn.apputility.flashlighthd;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class BaseActivity extends AppCompatActivity {

    private Tracker tracker;
    protected InterstitialAd mInterstitialAd;
    protected AdView mAdView;
    protected AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tracker = ((MyApplication) getApplication())
                .getTracker(MyApplication.TrackerName.APP_TRACKER);
        tracker.enableAdvertisingIdCollection(true);
        tracker.setScreenName("StartApp");
        tracker.send(new HitBuilders.AppViewBuilder().build());
        initFullScreenAds();
    }

    @Override
    public void onBackPressed() {

        if (!getSharedPreferences("rate", 0).getBoolean("rate", false)) {
            showDialogRating();
            getSharedPreferences("rate", 0).edit().putBoolean("rate", true).commit();
        } else {
            showDialog();
        }
    }


    public void showDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_dialog))
                .setCancelable(false)
                .setMessage(getString(R.string.exit_app))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (mInterstitialAd != null) {
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                finish();
                            }
                        } else {
                            finish();
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    protected void trackEvent(String event){
        tracker.send(new HitBuilders.EventBuilder().setCategory("UX")
                .setAction(event)
                .setLabel(event).build());
    }


    protected void initFullScreenAds(){
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.fullscreen_ad_unit_id));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                finish();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
            }
        });
        requestNewInterstitial();
    }
    protected void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);
    }


    protected void initBannerAds() {
        mAdView = (AdView) findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void showDialogRating() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_dialog))
                .setMessage(getString(R.string.rating_app))
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse("market://details?id=" + getPackageName()));
                        startActivity(i);
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        if (mInterstitialAd != null) {
                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                finish();
                            }
                        } else {
                            finish();
                        }
                    }
                })
                .show();
    }

}

package vn.apputility.flashlighthd.net;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import vn.apputility.flashlighthd.utils.Logger;

public class ApiRequest {


	private String				endpoint;
	private Map<String, String>	payload			= new HashMap<String, String>();
	private String				meta;

	private int					READ_TIMEOUT	= 10000;
	private int					CONNECT_TIMEOUT	= 10000;
	
//	public static void today(String[] args) {
//		System.out.println("secret "+secret);
//	}

	public ApiRequest(String endpoint, String sec) {

		this.endpoint = endpoint;
		try {
			URI uri = new URI(this.endpoint);
			this.endpoint = String.format("%s://%s%s", uri.getScheme(), uri.getHost(),
					uri.getPath());
			String query = uri.getQuery();
			if (query != null) {
				for (String param : query.split("&")) {
					String[] tmp = param.split("=");
					this.add(tmp[0], tmp[1]);
				}
			}
		} catch (URISyntaxException e) {
		} catch (Exception ex) {

		}

	}

	public void setReadTimeout(int timeout) {
		READ_TIMEOUT = timeout;
	}

	public void setConnectTimeout(int timeout) {
		CONNECT_TIMEOUT = timeout;
	}

	public void add(String name, String value) {
		if (value == null)
			value = "";

		payload.put(name, value);
	}

	public void addMeta(String meta) {
		this.meta = meta;
	}

	public ApiResponse get() {
		Logger.d("ApiResponse", "get ok");
		return request("GET");
	}

	public ApiResponse post() {
		return request("POST");
	}

	private ApiResponse request(String method) {
		Logger.d("ApiResponse", "request ok");
		/*
		 * StackTraceElement[] elems = Thread.currentThread().getStackTrace();
		 * String callingClass = null; for (StackTraceElement stackTraceElement
		 * : elems) { callingClass = stackTraceElement.getClassName(); }
		 * 
		 * if (callingClass == null || !callingClass.equals(allowedClass))
		 * return new ApiResponse(403, "Forbidden");
		 */

		HttpURLConnection conn = null;

		String queryString = getCanonicalizedQuery(method, payload);

		if (method.equals("GET")) {
			String uri = String.format("%s?%s", endpoint, queryString);
			try {
				URL url = new URL(uri);
				Logger.d("ApiRequest", "URL " + url);
				conn = (HttpURLConnection) url.openConnection();
			} catch (MalformedURLException e) {
				return new ApiResponse(502, e.getMessage());
			} catch (IOException e) {
				return new ApiResponse(502, e.getMessage());
			}
		} else if (method.equals("POST")) {
			// String uri = String.format("%s?%s", endpoint, queryString);
			meta = queryString;
			try {
				URL url = new URL(endpoint);
				conn = (HttpURLConnection) url.openConnection();
				Logger.d("ApiRequest", "URL " + url);
				System.out.println(("data " + meta));
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type",
						"application/x-www-form-urlencoded; application/json");

				conn.setRequestProperty("Content-Length",
						"" + Integer.toString(meta.getBytes().length));
				conn.setRequestProperty("Content-Language", "en-US");

				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				// Send request
				DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
				wr.writeBytes(meta);
				wr.flush();
				wr.close();
			} catch (MalformedURLException e) {
				return new ApiResponse(502, e.getMessage());
			} catch (IOException e) {
				return new ApiResponse(502, e.getMessage());
			}
		} else {
			return new ApiResponse(405, "Unknown method");
		}

		conn.setConnectTimeout(CONNECT_TIMEOUT);
		conn.setReadTimeout(READ_TIMEOUT);

		int statusCode = 502;
		String text = "";

		try {
			statusCode = conn.getResponseCode();
			Logger.d("ApiRequest", "status code " + statusCode);
		} catch (IOException e) {
			text = e.getMessage();
			Logger.d("ApiRequest", "IOException text " + text);
		}

		try {
			text = readStream(conn.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
			text = e.getMessage();
		}
		Logger.d("ApiResponse", "request final ok");
		return new ApiResponse(statusCode, text);
	}

	/**
	 * Examines the specified query string parameters and returns a
	 * canonicalized form.
	 * <p>
	 * 
	 * @param parameters
	 *            The query string parameters to be canonicalized.
	 * 
	 * @return A canonicalized form for the specified query string parameters.
	 */
	protected List<NameValuePair> getCanonicalizedQueryPairs(Map<String, String> parameters) {
		SortedMap<String, String> sorted = new TreeMap<String, String>();

		Iterator<Map.Entry<String, String>> pairs = parameters.entrySet().iterator();
		while (pairs.hasNext()) {
			Map.Entry<String, String> pair = pairs.next();
			String key = pair.getKey();
			String value = pair.getValue();
			sorted.put(key, value);
		}

		List<NameValuePair> query = new LinkedList<NameValuePair>();
		pairs = sorted.entrySet().iterator();
		while (pairs.hasNext()) {
			Map.Entry<String, String> pair = pairs.next();
			query.add(new BasicNameValuePair(pair.getKey(), pair.getValue()));
		}

		return query;
	}

	protected String getCanonicalizedQuery(String method, Map<String, String> parameters) {
		List<NameValuePair> params = getCanonicalizedQueryPairs(parameters);

		StringBuilder builder = new StringBuilder();

		Iterator<NameValuePair> pairs = params.iterator();
		boolean isFirst = true;
		while (pairs.hasNext()) {
			if (isFirst == false) {
				builder.append("&");
			} else {
				isFirst = false;
			}

			NameValuePair pair = (NameValuePair) pairs.next();
			try {
				builder.append(String.format("%s=%s", URLEncoder.encode(pair.getName(), "UTF-8"),
						URLEncoder.encode(pair.getValue(), "UTF-8")));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}

		String canonicalizedQueryString = builder.toString().replace("+", "%20");

		StringBuilder data = new StringBuilder();
		URI request = null;
		try {
			request = new URI(endpoint);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}

		data.append(method).append("\n");
		data.append(getCanonicalizedEndpoint(request)).append("\n");
		data.append(getCanonicalizedResourcePath(request)).append("\n");
		data.append(canonicalizedQueryString);

		String queryToSign = data.toString();
//		System.out.println("queryToSign "+queryToSign);
		return canonicalizedQueryString;
	}

	private String readStream(InputStream in) {
		StringBuilder builder = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(in));
			String line = "";
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return builder.toString();
	}

	/**
	 * Formats date as ISO 8601 timestamp
	 */
	private String getFormattedTimestamp() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		return df.format(getSignatureDate(0));
	}

	protected Date getSignatureDate(int timeOffset) {
		Date dateValue = new Date();
		if (timeOffset != 0) {
			long epochMillis = dateValue.getTime();
			epochMillis -= timeOffset * 1000;
			dateValue = new Date(epochMillis);
		}
		return dateValue;
	}

	private String getCanonicalizedResourcePath(URI request) {
		String resourcePath = "";

		if (request.getPath() != null) {
			resourcePath += request.getPath();
		}

		return resourcePath;
	}

	protected String getCanonicalizedEndpoint(URI endpoint) {
		String endpointForStringToSign = endpoint.getHost().toLowerCase();

		int port = endpoint.getPort();

		if (port != -1) {
			endpointForStringToSign += ":" + port;
		}

		return endpointForStringToSign;
	}
	
	/*public static void today(String[] args) {
		ApiRequest apiRequest = new ApiRequest("http://api.atiendayroi.com");
		apiRequest.add("username", "user");
		apiRequest.add("password", "pass");
		apiRequest.add("deviceId ", "898sgfwef908xcv");
		System.out.println(apiRequest.post().getText());
	}*/

}

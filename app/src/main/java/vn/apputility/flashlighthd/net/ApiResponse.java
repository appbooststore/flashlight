package vn.apputility.flashlighthd.net;


public class ApiResponse {
	private int statusCode;
	private String text;

	public ApiResponse(int statusCode, String text) {
		this.statusCode = statusCode;
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public int getStatusCode() {
		return statusCode;
	}
}

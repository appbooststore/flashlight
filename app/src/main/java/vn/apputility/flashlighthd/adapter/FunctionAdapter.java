package vn.apputility.flashlighthd.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import vn.apputility.flashlighthd.R;


/**
 * Created by Phan on 3/5/2016.
 */
public class FunctionAdapter extends BaseAdapter {
    private Context context;
    private TypedArray items;

    public FunctionAdapter(Context context, TypedArray items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length();
    }

    @Override
    public Object getItem(int position) {
        return items
                .getResourceId(position, -1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.item_function, null);
        }
        ImageView img = (ImageView) view.findViewById(R.id.img_function);
        img.setImageResource(items
                .getResourceId(position, -1));
        return view;
    }
}

package vn.apputility.flashlighthd;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import vn.apputility.flashlighthd.utils.ServerRunable;

import vn.apputility.flashlighthd.adapter.FunctionAdapter;
import vn.apputility.flashlighthd.model.UpdateDTO;
import vn.apputility.flashlighthd.utils.ApiAction;
import vn.apputility.flashlighthd.utils.Utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MainActivity extends BaseActivity implements View.OnClickListener, IBase, ServerRunable.CompleteListener {

//    private AppBoostTracking appBoost;
    private Camera camera;
    private boolean isFlashOn;
    private boolean hasFlash;
    private Camera.Parameters params;
    private Button btnPower;
    private Handler handler;
    private boolean statusFlash = true;
    private ToggleButton toggleTwink, toggleSOS;
    private TextView txt_percent, txt_tem;
    private GridView mGridView;
    private FunctionAdapter adapter;
    private TypedArray funcIcon;
    private ImageView btnFunction;
    private LinearLayout layout_led, layout_function, layout_sos, layout_twinkling;
    private int typeLayout = 0;
    private SeekBar speedOn, speedOff, speedSOS;
    private CheckBox loopCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        versionCode = BuildConfig.VERSION_CODE;
        requestData(ApiAction.NONE);
    /*    appBoost = MyApplication.analytics();
        appBoost.sendTracking();*/
/*
        AppBoostTracking.getInstance().trackingFirstLaunch(this);
*/
        hasFlash = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (!hasFlash) {

            AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
            alert.setTitle("Error");
            alert.setMessage("Sorry, your device doesn't support flash light!");
            alert.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    statusFlash = false;
                    turnOffFlash();
                    finish();
                }
            });
            alert.show();
            return;
        }

        getCamera();
        getPropertiesFromUI();
        setElementFromLayout();
        handler = new Handler();
        turnOnFlash();

        Utils.batteryLevel(MainActivity.this, txt_percent, txt_tem);


    }

    @Override
    public void getPropertiesFromUI() {
        btnPower = (Button) findViewById(R.id.power);
        txt_percent = (TextView) findViewById(R.id.txt_percent);
        txt_tem = (TextView) findViewById(R.id.txt_temperature);
        mGridView = (GridView) findViewById(R.id.mGridview);
        btnFunction = (ImageView) findViewById(R.id.btn_type);
        layout_led = (LinearLayout) findViewById(R.id.layout_led);
        layout_function = (LinearLayout) findViewById(R.id.layout_function);
        layout_twinkling = (LinearLayout) findViewById(R.id.layout_twinkling);
        layout_sos = (LinearLayout) findViewById(R.id.layout_sos);
        speedOn = (SeekBar) findViewById(R.id.speedON);
        speedOff = (SeekBar) findViewById(R.id.speedOFF);
        speedSOS = (SeekBar) findViewById(R.id.speedSOS);
        toggleSOS = (ToggleButton) findViewById(R.id.toggBtnSOS);
        toggleTwink = (ToggleButton) findViewById(R.id.toggBtnTwink);
        loopCheck = (CheckBox) findViewById(R.id.check_loop);
    }

    @Override
    public void setElementFromLayout() {
        btnPower.setOnClickListener(this);
        toggleSOS.setOnClickListener(this);
        toggleTwink.setOnClickListener(this);
        loopCheck.setOnClickListener(this);
        btnFunction.setOnClickListener(this);
        funcIcon = getResources().obtainTypedArray(
                R.array.arr_function_icon);
        adapter = new FunctionAdapter(MainActivity.this, funcIcon);
        mGridView.setAdapter(adapter);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                typeLayout = position;
                switch (position) {
                    case 0:
                        trackEvent("onclick FlashLight");
                        Utils.animLayout(MainActivity.this, layout_function, layout_led);
                        btnFunction.setImageResource(R.drawable.btn_type_off);
                        break;
                    case 1:
                        trackEvent("onclick Twinkling");
                        Utils.animLayout(MainActivity.this, layout_function, layout_twinkling);
                        btnFunction.setImageResource(R.drawable.btn_type_off);
                        break;
                    case 2:
                        trackEvent("onclick SOS");
                        Utils.animLayout(MainActivity.this, layout_function, layout_sos);
                        btnFunction.setImageResource(R.drawable.btn_type_off);
                        break;
                }
            }
        });
        initBannerAds();
    }

    private void getCamera() {

        if (camera == null) {
            try {
                camera = Camera.open();
                params = camera.getParameters();
            } catch (Exception e) {

            }
        }

    }

    private void turnOnFlash() {

        if (!isFlashOn) {
            if (camera == null || params == null) {
                return;
            }

            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();
            isFlashOn = true;
        }

    }

    private void turnOffFlash() {
        if (isFlashOn) {
            if (camera == null || params == null) {
                return;
            }

            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            isFlashOn = false;
        }
    }

    @Override
    protected void onDestroy() {
        statusFlash = false;
        turnOffFlash();
        if (camera != null) {
            camera.release();
            camera = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
/*
        turnOffFlash();
*/
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (isFlashOn)
            turnOnFlash();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isFlashOn)
            turnOnFlash();
        // on resume turn on the flash
    }

    @Override
    protected void onStart() {
        super.onStart();
        getCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // on stop release the camera

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.power:

                if (isFlashOn) {
                    turnOffFlash();
                    btnPower.setBackgroundResource(R.drawable.button_off);
                } else {
                    turnOnFlash();
                    btnPower.setBackgroundResource(R.drawable.button_on);
                }
                break;
            case R.id.toggBtnSOS:
                if (toggleSOS.isChecked()) {
                    statusFlash = true;
                    sosFlash();
                } else {
                    statusFlash = false;
                    turnOffFlash();
                }
                break;
            case R.id.toggBtnTwink:
                if (toggleTwink.isChecked()) {
                    statusFlash = true;
                    twinkFlash();
                } else {
                    statusFlash = false;
                    turnOffFlash();
                }
                break;
            case R.id.btn_type:
                statusFlash = false;
                toggleSOS.setChecked(false);
                toggleTwink.setChecked(false);
                btnPower.setBackgroundResource(R.drawable.button_off);
                turnOffFlash();
                if (layout_function.getVisibility() == View.VISIBLE) {
                    Utils.animLayout(MainActivity.this, layout_function, getLayout(typeLayout));
                    btnFunction.setImageResource(R.drawable.btn_type_off);
                } else {
                    Utils.animLayout(MainActivity.this, getLayout(typeLayout), layout_function);
                    btnFunction.setImageResource(R.drawable.btn_type);
                }

                break;
            case R.id.check_loop:
                statusFlash = false;
                turnOffFlash();
                break;
            default:
                break;
        }
    }

    /*SOS Flash*/
    private int shortSOS = 250;
    private int longSOS = 600;
    private int offSOS = 150;
    private int bwTime = 500;
    private int breakTime = 1000;
    private int location = 0;
    private int[] lifecycleSOS = new int[]{shortSOS, shortSOS, shortSOS, longSOS, longSOS, longSOS, shortSOS, shortSOS, shortSOS};

    private void sosFlash() {
        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (statusFlash) {

                        if (location == lifecycleSOS.length && loopCheck.isChecked()) {
                            location = 0;
                        } else if (location == lifecycleSOS.length && !loopCheck.isChecked()) {
                            statusFlash = false;
                            turnOffFlash();
                            toggleSOS.setChecked(false);
                            return;
                        }
                        ;
                        if (isFlashOn) {
                            turnOffFlash();
                            if (location == lifecycleSOS.length - 1) {
                                handler.postDelayed(this, breakTime - (speedSOS.getProgress() * 20));
                            } else {
                                if (location == 2 || location == 5) {
                                    handler.postDelayed(this, bwTime);
                                } else {
                                    handler.postDelayed(this, offSOS);
                                }
                            }
                            location++;
                        } else {
                            turnOnFlash();
                            handler.postDelayed(this, lifecycleSOS[location] - (speedSOS.getProgress() * 20));
                        }
                    }
                }
            }, 300);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*Twinkling Flash*/
    private void twinkFlash() {
        try {
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (statusFlash) {
                        if (isFlashOn) {
                            turnOffFlash();
                            handler.postDelayed(this, 250 - (speedOff.getProgress() * 20));
                        } else {
                            turnOnFlash();
                            handler.postDelayed(this, 250 - (speedOn.getProgress() * 20));
                        }
                    }
                }
            }, 300);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LinearLayout getLayout(int typeLayout) {
        switch (typeLayout) {
            case 0:
                return layout_led;
            case 1:
                return layout_twinkling;
            case 2:
                return layout_sos;
        }
        return null;
    }


    /*check update*/
    private int versionCode = 0;
    public static final int progress_bar_type = 0;
    private static String file_url = "";
    private ProgressDialog pDialog;

    @Override
    public void onComplete(Object values) {
        if (values instanceof UpdateDTO) {
            UpdateDTO updateDTO = (UpdateDTO) values;
            if ("1".equalsIgnoreCase(updateDTO.update)) {
                if (updateDTO.version != null) {
                    if (Integer.parseInt(updateDTO.version) > versionCode) {
                        file_url = updateDTO.link;
                        if ("1".equalsIgnoreCase(updateDTO.force)) {
                            showDialogUpdate(true);
                        } else {
                            showDialogUpdate(false);
                        }
                        return;
                    }
                }

            }
        }
    }

    public void requestData(ApiAction apiAction) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        ServerRunable runable = new ServerRunable(MainActivity.this, executorService);
        runable.getRequet(apiAction);
        runable.setListener(MainActivity.this);
        executorService.execute(runable);
    }

    public void showDialogUpdate(final boolean force) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getString(R.string.title_dialog_update))
                .setMessage(getString(R.string.message))
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        new DownloadFileFromURL().execute(file_url);
                        dialog.dismiss();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (force) {
                            finish();
                        } else {
                            dialog.dismiss();
                        }
                    }
                })

                .show();
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage(getString(R.string.title_dialog_progress));
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                File mydownload = new File(Environment.getExternalStorageDirectory()
                        .getPath() + "/" + getString(R.string.app_name) + "/");
                if (!mydownload.exists()) {
                    mydownload.mkdir();
                }

                URL url = new URL(file_url);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                String fileName = file_url.substring(file_url.lastIndexOf('/') + 1);
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory()
                        .getPath() + "/" + getString(R.string.app_name) + "/" + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    output.write(data, 0, count);
                }

                output.flush();

                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }


        protected void onProgressUpdate(String... progress) {
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String a) {
            dismissDialog(progress_bar_type);
            String fileName = file_url.substring(file_url.lastIndexOf('/') + 1);
            File apkFile = new File(Environment.getExternalStorageDirectory()
                    .getPath() + "/" + getString(R.string.app_name) + "/" + fileName);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.fromFile(apkFile),
                    "application/vnd.android.package-archive");
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            getApplication().startActivity(intent);
            finish();
        }

    }




}